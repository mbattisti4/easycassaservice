package it.easycassa.easycassaservice.helpers;

public class Constants {
    public static final String TAG = "EasyCassa_BG_Service";

    public static String rpSERVER_FILE_SUBFOLDER = "Android/";
    public static String rpSERVER_VERSION_FILE_TXT = "VersioneAPK.txt";
    public static String rpSERVER_VERSION_FILE_JSON = "VersioneAPK.json";
    public static String rpSERVER_APK_FILE = "Easycassa_V%d.apk";
    public static String rpAPIBaseURL_TEST = "https://api.easycassa.it/";
    public static String PACKAGE_NAME = "it.sisal.easycassa";

    public static String rpUPDATE_AVAILABLE_BROADCAST = "it.sisal.easycassa.apk_update_available";
    public static String rpDO_UPDATE = "it.sisal.easycassa.do_apk_update";
}
