package it.easycassa.easycassaservice;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import it.easycassa.easycassaservice.helpers.Constants;

import static it.easycassa.easycassaservice.helpers.Constants.TAG;
import static it.easycassa.easycassaservice.helpers.Constants.rpAPIBaseURL_TEST;
import static it.easycassa.easycassaservice.helpers.Constants.rpDO_UPDATE;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(Constants.TAG, "Starting app");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent serviceIntent = new Intent(getApplicationContext(), EasyCassaBackgroundService.class);
        serviceIntent.putExtra("download_url", rpAPIBaseURL_TEST);

        startService(serviceIntent);

        boolean sentAppToBackground = moveTaskToBack(true);
        Log.d(Constants.TAG, "sentAppToBackground:" + sentAppToBackground);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
