package it.easycassa.easycassaservice;

import android.app.DownloadManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


import static it.easycassa.easycassaservice.helpers.Constants.*;

public class EasyCassaBackgroundService extends Service {
    private long idFileServerVersion;
    private long idFileApkApp;

    private String pathRemoteVersionFile;
    //private String pathLocalVersionFile;
    private String pathRemoteApk;
    private int serverAPKVersion;
    private int currentAPKVersion;
    private String baseURL;
    private String apkInternalPath;
    private boolean mustInstall;
    BroadcastReceiver onDownloadComplete;
    BroadcastReceiver onDoUpdateRequest;
    PackageManager packageManager;

    public EasyCassaBackgroundService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        this.onDownloadComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.d(TAG, "onDownloadComplete - Broadcast message received: " + action);

                long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

                if (idFileServerVersion == id) {
                    serverAPKVersion = _readServerVersion();
                    pathRemoteApk = baseURL + rpSERVER_FILE_SUBFOLDER + String.format(rpSERVER_APK_FILE, serverAPKVersion);

                    if (currentAPKVersion < serverAPKVersion) {

                        // se la versione dell'apk è minore di quella sul server invio un broadcast
                        final Intent i = new Intent();
                        i.putExtra("serverVersion", serverAPKVersion);
                        i.setAction(rpUPDATE_AVAILABLE_BROADCAST);
                        i.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                        getApplicationContext().sendBroadcast(i);
                        Log.d(TAG, "onDownloadComplete - Broadcast message sent: " + rpUPDATE_AVAILABLE_BROADCAST);

                    }
                    if (mustInstall) {
                        apkInternalPath = _downloadAppApk(pathRemoteApk);
                        mustInstall = false;
                    }
                }
                if (idFileApkApp == id) {
                    //Toast.makeText(getApplicationContext(), "Download APK Completed", Toast.LENGTH_SHORT).show();
                    _installApk(apkInternalPath);
                    _startNewActivity(getApplicationContext(), PACKAGE_NAME);
                }
            }
        };

        this.onDoUpdateRequest = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.d(TAG, "onDoUpdateRequest - Broadcast message received: " + action);

                apkInternalPath = _downloadAppApk(pathRemoteApk);
                Log.d(TAG, "onDoUpdateRequest - apkInternalPath: " + apkInternalPath);
            }
        };


        // Registers the receiver so that your service will listen for
        registerReceiver(onDownloadComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        registerReceiver(onDoUpdateRequest, new IntentFilter(rpDO_UPDATE));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        baseURL = intent.getStringExtra("download_url");
        Log.d(TAG, "onStartCommand - Service started - base URL: " + baseURL);
        pathRemoteVersionFile = baseURL + rpSERVER_FILE_SUBFOLDER + rpSERVER_VERSION_FILE_TXT;
        packageManager = this.getPackageManager();

        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        if (_checkIfAppExists(PACKAGE_NAME, packageManager))
                            mustInstall = false;
                        else
                            mustInstall = true;

                        _checkServerVersion(pathRemoteVersionFile);

                        Thread.sleep(20000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(TAG, "onTaskRemoved - Service removed");
        unregisterReceiver(onDownloadComplete);
        unregisterReceiver(onDoUpdateRequest);

        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());
        startService(restartServiceIntent);
        super.onTaskRemoved(rootIntent);
    }

    private void _checkServerVersion(String pathRemoteVersionFile) {
        File file = new File(getExternalFilesDir(null), rpSERVER_VERSION_FILE_TXT);

        if (file.exists())
            file.delete();

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(pathRemoteVersionFile))
                .setTitle("Dummy File")// Title of the Download Notification
                .setDescription("Downloading")// Description of the Download Notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)// Visibility of the download Notification
                .setDestinationUri(Uri.fromFile(file))// Uri of the destination file
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true);// Set if download is allowed on roaming network


        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        //Log.d(TAG, "onStartCommand - Starting download");
        idFileServerVersion = downloadManager.enqueue(request);// enqueue puts the download request in the queue.
        //Log.d(TAG, "onStartCommand - downloadID: " + idFileServerVersion);
    }

    private int _readServerVersion() {
        File file = new File(getExternalFilesDir(null), rpSERVER_VERSION_FILE_TXT);

        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
            }
            br.close();

            Log.d(TAG, "_readServerVersion - value read: " + text.toString());
            return Integer.parseInt(text.toString());
        } catch (IOException e) {
            Log.d(TAG, "_readServerVersion - error: " + e.getMessage());
            return -1;
        }
    }

    private String _downloadAppApk(String pathRemoteApkFile) {
        String apkFileName = String.format(rpSERVER_APK_FILE, serverAPKVersion);
        File file = new File(getExternalFilesDir(null), apkFileName);

        if (file.exists())
            file.delete();

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(pathRemoteApkFile))
                .setTitle(apkFileName)// Title of the Download Notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)// Visibility of the download Notification
                .setDestinationUri(Uri.fromFile(file))// Uri of the destination file
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true);// Set if download is allowed on roaming network


        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        idFileApkApp = downloadManager.enqueue(request);// enqueue puts the download request in the queue.
        return file.getAbsolutePath();
    }

    private void _installApk(String apkInternalPath) {
        try {
            String command = "pm install -r " + apkInternalPath; // + " && am start -n " + PACKAGE_NAME + "/" + PACKAGE_NAME + ".MainActivity";
            Log.d(TAG, "command: " + command);
            Process p = Runtime.getRuntime().exec(command);
            try {
                p.waitFor();
            } catch (InterruptedException e) {
                Log.e(TAG, "_installApk - ERROR - : " + e.getMessage());
            }


        } catch (IOException e) {
            Log.e(TAG, e.toString());
        }
    }

    public void _startNewActivity(Context context, String packageName) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent != null) {
            // We found the activity now start the activity
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    private boolean _checkIfAppExists(String packageName, PackageManager packageManager) {
        try {
            PackageInfo pinfo = packageManager.getPackageInfo(packageName, 0);

            currentAPKVersion = pinfo.versionCode;
            Log.d(TAG, "_checkIfAppExists:Current easycassa apk version: " + currentAPKVersion);
            String verName = pinfo.versionName;

            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
